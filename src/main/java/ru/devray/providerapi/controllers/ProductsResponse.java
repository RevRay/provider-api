package ru.devray.providerapi.controllers;

import lombok.Data;
import ru.devray.providerapi.products.Product;

import java.util.List;

@Data
public class ProductsResponse {
  private final List<Product> products;
}
